#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "queue.h"

int empty(struct queue_t * q) {
	return (q->size == 0);
}

void enqueue(struct queue_t * q, struct pcb_t * proc) {
	/* TODOO: put a new process to queue [q] */	
    if(q->size >= MAX_QUEUE_SIZE)
        return;
    q->proc[q->size] = proc;
    q->size ++;   
}

struct pcb_t * dequeue(struct queue_t * q) {
	/* TODOO: return a pcb whose prioprity is the highest
	 * in the queue [q] and remember to remove it from q
	 * */
    if(empty(q))
        return NULL;
    
    struct pcb_t * h = q->proc[0];
    int idx = 0;

    for(int i = 1; i < q->size; i ++){
        if(q->proc[i]->priority < h->priority){
            h = q->proc[i]; idx = i;
        }
    }
    int j;
    for(j = idx; j < q->size - 1; j ++){
        q->proc[j] = q->proc[j+1];
    }

    q->proc[j+1] = NULL;    

    q->size --;
    return h;
}

