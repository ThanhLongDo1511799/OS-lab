#include <stdio.h>
#include <stdlib.h>
#include "queue.h"

int empty(struct queue_t * q) {
	return (q->size == 0);
}

void enqueue(struct queue_t * q, struct pcb_t * proc) {
	/* TODO: put a new process to queue [q] */
	struct pcb_t *newProc = proc;
	q->proc[q->size] = newProc;
	q->size++;
}

struct pcb_t * dequeue(struct queue_t * q) {
	/* TODO: return a pcb whose prioprity is the highest
	 * in the queue [q] and remember to remove it from q
	 * */
	 //->I do
	 //Create item that will be returned
	struct pcb_t * proc = NULL;

	if (empty(q))
		return NULL;

	//Find the process that have the highest priority
	int i;
	int indexOfMaxProc = 0;
	int priOfMaxProc = q->proc[0]->priority;
	for (i = 1; i < q->size; i++) {
		if (q->proc[i]->priority > priOfMaxProc) {
			indexOfMaxProc = i;
			priOfMaxProc = q->proc[i]->priority;
		}
	}
	//Assign this process to item to return 
	proc = q->proc[indexOfMaxProc];

	//Remove it from q
	//Swap last item to new max size item.
	q->proc[indexOfMaxProc] = q->proc[q->size - 1];
	q->size--;
	return proc;
}

