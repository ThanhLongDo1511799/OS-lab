#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#define NUM_THREAD 5

static volatile int circle=0;
pthread_mutex_t lock;
void *pi(void *a)
{	
	int t;
	int loop=*(int *)a;
	for(t=0;t<loop;t++){
		double x,y;
		if(rand()%2==0)
			x=(rand()%101)*0.01;
		else
			x=-(rand()%101)*0.01;

		if(rand()%2==0)
			y=(rand()%101)*0.01;
		else
			y=-(rand()%101)*0.01;
		
		pthread_mutex_lock(&lock);
		if(sqrt(x*x+y*y)<1)
			circle++;

		pthread_mutex_unlock(&lock);
		//printf("%lf\n",sqrt(x*x+y*y));
		//printf("x=%lf, y=%lf \n",x,y);
	}


	pthread_exit(0);
}
int main(int argc, char *argv[]){
	clock_t begin=clock();
	
	srand(time(NULL));
	pthread_t threads[NUM_THREAD];
	int all_point=atoi(argv[1]);
	int param=all_point/NUM_THREAD;
	
	int t;
	for(t=0;t<NUM_THREAD;t++){
		pthread_create(&threads[t],NULL,pi,&param);
	}
	
	for(t=0;t<NUM_THREAD;t++)	
		pthread_join(threads[t],NULL);	
	
	clock_t end=clock();
	double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	
	printf("time to run: %lf\n", time_spent);
	printf("circle : %d\n",circle);
	printf("pi: %lf\n",4.0*(double)circle/(double)all_point);
	printf("exit in main\n");
	return 0;

}

