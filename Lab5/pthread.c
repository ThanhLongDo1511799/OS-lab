#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#define NUM_THREADS 5

void *PrintHello(void *threadid)
{
	long tid;
	tid =(long)threadid;
	printf("Hello world!it's me, thread %ld\n",tid);
	pthread_exit(NULL);
}

int main(int argc, char *argv[])
{
	pthread_t threads[NUM_THREADS];
	int rc;
	long t;
	for(t=0;t<NUM_THREADS;t++){
		printf("in main: creating thread %ld\n",t);
		rc = pthread_create(&threads[t],NULL,PrintHello,(void *)t);
		//pthread_join(threads[t],NULL);
	if(rc){
		printf("error, return from pthread create() is %d\n",rc);
	exit(-1);	
	}
	}
	
	for(t=0;t<NUM_THREADS;t++){
		pthread_join(threads[t],NULL);
	}

	printf("main thread exitting!!!\n");
	pthread_exit(NULL);
	return 0;
}
