//#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

int circle=0;
int pi(int a)
{	
	int t;
	for(t=0;t<a;t++){
		double x,y;
		if(rand()%2==0)
			x=(rand()%101)*0.01;
		else
			x=-(rand()%101)*0.01;

		if(rand()%2==0)
			y=(rand()%101)*0.01;
		else
			y=-(rand()%101)*0.01;

		if(sqrt(x*x+y*y)<1)
			circle++;
		//printf("%lf\n",sqrt(x*x+y*y));
		//printf("x=%lf, y=%lf \n",x,y);
	}
	return 0;
}
int main(int argc, char *argv[]){
	clock_t begin=clock();	
	
	srand(time(NULL));
	int a=atoi(argv[1]);
	
	pi(a);
	
	clock_t end=clock();
	double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;

	printf("pi= \t %lf\n",(double)circle*4.0/(double)a);
	printf("time to run: %lf\n", time_spent);
	return 0;

}
