#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#define NUM_THREAD 100

struct container{
	int point_generate;
	int point_circle;
};

void *pi(void *a)
{	
	int t;
	struct container * str =(struct container *)a;
	int loop=str->point_generate;
	for(t=0;t<loop;t++){
		double x,y;
		if(rand()%2==0)
			x=(rand()%101)*0.01;
		else
			x=-(rand()%101)*0.01;

		if(rand()%2==0)
			y=(rand()%101)*0.01;
		else
			y=-(rand()%101)*0.01;

		if(sqrt(x*x+y*y)<1)
			(str->point_circle)++;
	}
	pthread_exit(0);
}
int main(int argc, char *argv[]){
	clock_t begin=clock();
	
	srand(time(NULL));
	pthread_t threads[NUM_THREAD];
	int all_point=atoi(argv[1]);
	struct container param[NUM_THREAD];
	int circle=0;
	
	int t;
	for(t=0;t<NUM_THREAD;t++){
		param[t].point_generate=all_point/NUM_THREAD;
		param[t].point_circle=0;
		pthread_create(&threads[t],NULL,pi,&param[t]);
	}
	
	for(t=0;t<NUM_THREAD;t++)	
		pthread_join(threads[t],NULL);	
	
	for(t=0;t<NUM_THREAD;t++){
		circle+=param[t].point_circle;
	}
	
	clock_t end=clock();
	double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	
	printf("time to run: %lf\n", time_spent);
	printf("circle : %d\n",circle);
	printf("pi: %lf\n",4.0*(double)circle/(double)all_point);
	printf("exit in main\n");
	return 0;
}
