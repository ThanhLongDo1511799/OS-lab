#ifndef STRUCTS_H
#define STRUCTS_H

#include <pthread.h>

//the PCB of a process
struct pcb_t{
	//values initialized for each process
	int arrival_time;//the timestamp at which process arrives
			//and wishs to start
	int burst_time;//the amount of time that process requires
			//to complete its job
	int pid;	//process id
}

//'wrapper of pcb in a queue
struct qitem_t{
	struct pcb_t *data;
	struct qitem_t *next;
};

//the 'queue' used for both ready queue and in_queue(e.g. the list of)
//process that will loaded in the future

struct pqueue_t{
	//HEAD and TAIL for queue
	struct qitem_t *head;
	struct qitem_t *tail;
	//MUTEX used to protect the queue from
	//being modified by multiple threads
	pthread_mutex_t lock;
}
#endif
