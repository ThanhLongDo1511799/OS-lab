#include "queue.h"
#include <pthread.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#define TIME-UNIT	100//microsecond

static struct pqueue_t in_queue;//queue for incomming process
static struct pqueue_t ready_queue;//queue for ready process

static int load_done=0;

static int timeslot;//the maximum amount of time a process is alolowed
//to be run on cpu before being swapped out

//emulate the cpu
void *cpu(void * arg);

//Emulate the loader
void *loader(void *arg);

//read the list of process to be executed from stdin
void load_task();

int main(){
	pthread_t cpu_id;//cpu id
	pthread_t loader_id;//loader id

	//initialize queues
	initialize_queue(&in_queue);
	initialize_queue(&ready_queue);
	
	//Read a list of jobs to be run
	load_task();

	//start cpu
	pthread_create(&cpu_id,NULL, cpu, NULL);

	//start loader
	pthread_create(&loader_id,NULL, loader, NULL);

	//wait for cpu and loader
	pthread_join(cpu_id,NULL);
	pthread_join(loader_id,NULL);

	pthread_exit(NULL);
}


