//ThanhLongDo-1511799
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

int main(){
FILE *fp;
int a[100];
int size=0;
int previous='\n',temp;
pid_t pid;
int count=0;

//-----------------------
//read file and fetch data to array
//----------------------

//open file
   fp = fopen("numbers.txt", "r");

do {
      temp = fgetc(fp);
	if(previous=='\n'&&temp =='\n'){
	//do nothing
	}
      else if (temp == '\n'){
	previous=temp;
	size++;
	}
	else{
	previous =temp;
	}
	
} while (temp != EOF);

 printf("the line number of file: %d\n", size);

//close file   
fclose(fp);

//open file
   fp = fopen("numbers.txt", "r");

for(int i=0;i<size;i++){
 fscanf(fp, "%d", &a[i]);
}

for(int i=0;i<size;i++){
 printf("%d\n", a[i]);
}

//close file   
fclose(fp);


//----------------------------
//use fork to create process
//----------------------------

pid=fork();

if(pid<0){//error
perror ( " fork " ) ;
exit(1);
}
else if(pid==0){//child process
	for(int i=0;i<size;i++){
		if(a[i]%3==0)	count++;
	}

printf("the quality of number which is divisible by 3 is: %d\n",count);
}
else{//parent process
for(int i=0;i<size;i++){
		if(a[i]%2==0)	count++;
}

printf("the quality of number which is divisible by 2 is: %d\n",count);
}

return 0;
}
